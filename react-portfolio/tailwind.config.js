/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        amelaryas: 'amelaryas',
        burtons: 'burtons',
        camaly: 'camaly',
        roboto: 'roboto',
        voltdeco: 'voltdeco',
      },
      colors: {
        indigo: {
          950: '#070624',
        },
        blue: {
          950: '#0e0d2b',
        },
        purple: {
          950: '#cbcafc',
        },
        red: {
          950: '#192c4d',
        },
        boxShadow: {
          '3xl': '0 35px 60px -15px rgba(255, 255, 255, 0.3)',
        }
      }
    },
  },
  plugins: [],
}

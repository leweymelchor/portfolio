import 'leaflet/dist/leaflet.css'
import style from '../../styles/Home.module.css'
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import L from "leaflet";

function Map() {
    const position = [33.4484, -112.0740];

    const customMarker = new L.icon({
        iconUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-icon.png",
        iconSize: [25, 41],
        iconAnchor: [10, 41],
        popupAnchor: [2, -40]
    });

    return (

        <MapContainer className={style.map} center={position} zoom={10} scrollWheelZoom={true}>
            <TileLayer attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
            <Marker icon={customMarker}
                key={position}
                position={position}>
                <Popup>
                    My current city <br /> I am happy to relocate!
                </Popup>
            </Marker>
        </MapContainer>
    );
}

export default Map;
